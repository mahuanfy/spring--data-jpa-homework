package com.twuc.webApp.domain.controller;

import com.twuc.webApp.domain.entity.Product;
import com.twuc.webApp.domain.entity.ProductRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/products")
public class ProductController {
    private final ProductRepository productRepository;

    public ProductController(ProductRepository productRepository) {
        this.productRepository = productRepository;
    }

    @GetMapping
    public ResponseEntity getProducts() {
        List<Product> products = productRepository.findAllByOrderByProductCode();
        return ResponseEntity.ok().body(products);
    }

    @GetMapping("/pages")
    public ResponseEntity getProductsPage(
            @RequestParam(defaultValue = "0") int page,
            @RequestParam(defaultValue = "10") int size,
            @RequestParam(defaultValue = "ASC") String direction,
            @RequestParam(defaultValue = "productCode") String shot) {
        PageRequest pageRequest = PageRequest.of(page, size, Sort.Direction.valueOf(direction.toUpperCase()), shot);
        Page<Product> products = productRepository.findAll(pageRequest);
        return ResponseEntity.ok().body(products);
    }

    @PostMapping
    public ResponseEntity addProduct(@RequestBody Product product) {
        productRepository.save(product);
        return ResponseEntity.status(HttpStatus.CREATED).build();
    }
}

package com.twuc.webApp.domain.controller;

import com.twuc.webApp.domain.entity.Product;
import com.twuc.webApp.domain.entity.ProductLineRepository;
import com.twuc.webApp.domain.entity.ProductLine;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/productLines")
public class ProductLineController {
    private final ProductLineRepository productLineRepository;

    public ProductLineController(ProductLineRepository productLineRepository) {
        this.productLineRepository = productLineRepository;
    }

    @GetMapping
    public ResponseEntity getProductLines() {
        List<ProductLine> productLines = productLineRepository.findAllByOrderByProductLine();
        return ResponseEntity.ok().body(productLines);
    }

    @GetMapping("/pages")
    public ResponseEntity getProductLinesPage(
            @RequestParam(defaultValue = "0") int page,
            @RequestParam(defaultValue = "10") int size,
            @RequestParam(defaultValue = "ASC") String direction,
            @RequestParam(defaultValue = "productLine") String shot) {
        PageRequest pageRequest = PageRequest.of(page, size, Sort.Direction.valueOf(direction.toUpperCase()), shot);
        Page<ProductLine> productLines = productLineRepository.findAll(pageRequest);
        return ResponseEntity.ok().body(productLines);
    }

    @PostMapping
    public ResponseEntity addProduct(@RequestBody ProductLine productLine) {
        productLineRepository.save(productLine);
        return ResponseEntity.status(HttpStatus.CREATED).build();
    }
}

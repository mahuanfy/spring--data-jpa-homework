package com.twuc.webApp.domain.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

@Entity(name = "productlines")
public class ProductLine {
    @Id
    @Column(name = "productLine", length = 50)
    private String productLine;
    @Column(name = "textDescription", length = 4000)
    private String textDescription;

    public ProductLine(String productLine, String textDescription) {
        this.productLine = productLine;
        this.textDescription = textDescription;
    }

    public ProductLine() {
    }

    public String getProductLine() {
        return productLine;
    }

    public String getTextDescription() {
        return textDescription;
    }
}

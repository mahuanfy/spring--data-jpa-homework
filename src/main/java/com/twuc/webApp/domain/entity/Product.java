package com.twuc.webApp.domain.entity;

import javax.persistence.*;
import java.math.BigDecimal;

@Entity(name = "products")
public class Product {
    @Id
    @Column(name = "productCode", length = 15)
    private String productCode;
    @Column(name = "productName", length = 70)
    private String productName;
    @Column(name = "productScale", length = 10)
    private String productScale;
    @Column(name = "productVendor", length = 50)
    private String productVendor;
    @Column(name = "productDescription", columnDefinition="TEXT")
    private String productDescription;
    @Column(name = "quantityInStock")
    private Short quantityInStock;
    @Column(name = "buyPrice")
    private BigDecimal buyPrice;
    @Column(name = "MSRP")
    private BigDecimal MSRP;
    @ManyToOne
    @JoinColumn(name = "productLine")
    private ProductLine productLine;

    public Product(String productCode, String productName, String productScale, String productVendor, String productDescription, Short quantityInStock, BigDecimal buyPrice, BigDecimal MSRP) {
        this.productCode = productCode;
        this.productName = productName;
        this.productScale = productScale;
        this.productVendor = productVendor;
        this.productDescription = productDescription;
        this.quantityInStock = quantityInStock;
        this.buyPrice = buyPrice;
        this.MSRP = MSRP;
    }

    public Product() {
    }

    public void setProductLine(ProductLine productLine) {
        this.productLine = productLine;
    }

    public String getProductCode() {
        return productCode;
    }

    public String getProductName() {
        return productName;
    }

    public ProductLine getProductLine() {
        return productLine;
    }

    public String getProductScale() {
        return productScale;
    }

    public String getProductVendor() {
        return productVendor;
    }

    public String getProductDescription() {
        return productDescription;
    }

    public Short getQuantityInStock() {
        return quantityInStock;
    }

    public BigDecimal getBuyPrice() {
        return buyPrice;
    }

    public BigDecimal getMSRP() {
        return MSRP;
    }
}

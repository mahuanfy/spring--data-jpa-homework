package com.twuc.webApp.domain.entity;

import com.twuc.webApp.domain.ApiTestBase;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.web.servlet.MockMvc;

import java.math.BigDecimal;

import static org.junit.jupiter.api.Assertions.assertEquals;

class ProductTest extends ApiTestBase {
    @Autowired
    private ProductRepository productRepository;
    @Autowired
    private ProductLineRepository productLineRepository;
    @Autowired
    private MockMvc mockMvc;
    @Test
    void productAndProductLineIsManyToOne() {
        flushAndClear(()->{
            Product product = new Product("S10_1949", "1969 Harley Davidson Ultimate Chopper", "1:10", "Min Lin Diecast", "This replica features working kickstand, front suspension, gear-shift lever, footbrake lever, drive chain, wheels and steering. All parts are particularly delicate due to their precise scale and require special care and attention.", (short) 7305, BigDecimal.valueOf(98.58), BigDecimal.valueOf(214.30));
            ProductLine productLine = new ProductLine("Motorcycles", "Our");
            productLineRepository.save(productLine);
            product.setProductLine(productLine);
            productRepository.save(product);
        });
        flushAndClear(()->{
            Product product = productRepository.findAll().get(0);
            assertEquals("Motorcycles", product.getProductLine().getProductLine());
            assertEquals("Our", product.getProductLine().getTextDescription());
        });
    }
}
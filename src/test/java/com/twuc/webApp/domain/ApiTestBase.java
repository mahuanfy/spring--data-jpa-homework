package com.twuc.webApp.domain;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;

@AutoConfigureMockMvc
@SpringBootTest
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_EACH_TEST_METHOD)
@Transactional
public abstract class ApiTestBase {
    @Autowired
    private MockMvc mockMvc;
    @Autowired
    private EntityManager em;
    protected void flushAndClear(Runnable runnable){
        runnable.run();
        em.flush();
        em.clear();
    }

    protected MockMvc getMockMvc() {
        return mockMvc;
    }
}
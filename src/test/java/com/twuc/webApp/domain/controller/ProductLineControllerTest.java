package com.twuc.webApp.domain.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.twuc.webApp.domain.ApiTestBase;
import com.twuc.webApp.domain.entity.Product;
import com.twuc.webApp.domain.entity.ProductLine;
import com.twuc.webApp.domain.entity.ProductLineRepository;
import com.twuc.webApp.domain.entity.ProductRepository;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MvcResult;

import java.math.BigDecimal;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

class ProductLineControllerTest extends ApiTestBase {
    @Autowired
    private ProductRepository productRepository;
    @Autowired
    private ProductLineRepository productLineRepository;

    @Test
    void getProductLines() throws Exception {
        initData();
        getMockMvc().perform(get("/api/productLines"))
                .andExpect(status().is(200))
                .andExpect(jsonPath("$[0].productLine").value("Classic Cars"));
    }
    @Test
    void getProductsPages() throws Exception {
        initData();
        getMockMvc().perform(get("/api/productLines/pages?page=0&size=10&sort=productLine&direction=asc"))
                .andExpect(status().is(200))
                .andExpect(jsonPath("$.content[0].productLine").value("Classic Cars"))
                .andExpect(jsonPath("$.totalPages").value(1));
    }
    @Test
    void addProductLine() throws Exception {
        ProductLine productLine = new ProductLine("Classic Cars", "Our");
        ObjectMapper objectMapper = new ObjectMapper();
        String productLineJson = objectMapper.writeValueAsString(productLine);

        getMockMvc().perform(post("/api/productLines")
                .content(productLineJson).contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(status().is(201));
    }
    void initData(){
        Product product = new Product("S10_1949", "1969 Harley Davidson Ultimate Chopper", "1:10", "Min Lin Diecast", "This replica features working kickstand, front suspension, gear-shift lever, footbrake lever, drive chain, wheels and steering. All parts are particularly delicate due to their precise scale and require special care and attention.", (short) 7305, BigDecimal.valueOf(98.58), BigDecimal.valueOf(214.30));
        ProductLine productLine = new ProductLine("Classic Cars", "Our");
        productLineRepository.save(productLine);
        product.setProductLine(productLine);
        productRepository.save(product);
    }
}
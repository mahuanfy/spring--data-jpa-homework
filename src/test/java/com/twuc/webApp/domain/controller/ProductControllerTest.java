package com.twuc.webApp.domain.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.twuc.webApp.domain.ApiTestBase;
import com.twuc.webApp.domain.entity.Product;
import com.twuc.webApp.domain.entity.ProductLine;
import com.twuc.webApp.domain.entity.ProductLineRepository;
import com.twuc.webApp.domain.entity.ProductRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;

import java.math.BigDecimal;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

class ProductControllerTest extends ApiTestBase {
    @Autowired
    private ProductRepository productRepository;
    @Autowired
    private ProductLineRepository productLineRepository;

    @Test
    void getProducts() throws Exception {
        initData();
        getMockMvc().perform(get("/api/products"))
                .andExpect(status().is(200))
                .andExpect(jsonPath("$[0].productCode").value("S10_1949"));
    }
    @Test
    void getProductsPages() throws Exception {
        initData();
        getMockMvc().perform(get("/api/products/pages?page=0&size=10&sort=productCode&direction=asc"))
                .andExpect(status().is(200))
                .andExpect(jsonPath("$.content[0].productCode").value("S10_1949"))
                .andExpect(jsonPath("$.totalPages").value(1));
    }

    @Test
    void addProduct() throws Exception {
        Product product = new Product("S10_1949", "1969 Harley Davidson Ultimate Chopper", "1:10", "Min Lin Diecast", "This replica features working kickstand, front suspension, gear-shift lever, footbrake lever, drive chain, wheels and steering. All parts are particularly delicate due to their precise scale and require special care and attention.", (short) 7305, BigDecimal.valueOf(98.58), BigDecimal.valueOf(214.30));
        ObjectMapper objectMapper = new ObjectMapper();
        String productJson = objectMapper.writeValueAsString(product);

        getMockMvc().perform(post("/api/products")
                .content(productJson).contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(status().is(201));
    }
    void initData(){
        Product product = new Product("S10_1949", "1969 Harley Davidson Ultimate Chopper", "1:10", "Min Lin Diecast", "This replica features working kickstand, front suspension, gear-shift lever, footbrake lever, drive chain, wheels and steering. All parts are particularly delicate due to their precise scale and require special care and attention.", (short) 7305, BigDecimal.valueOf(98.58), BigDecimal.valueOf(214.30));
        ProductLine productLine = new ProductLine("Motorcycles", "Our");
        productLineRepository.save(productLine);
        product.setProductLine(productLine);
        productRepository.save(product);
    }
}